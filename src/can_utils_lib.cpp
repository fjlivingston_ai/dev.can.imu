#include "can_utils_lib.h"

namespace mechaspin
{
namespace coms
{

int CAN_Utils::writeFrame(struct can_frame frame)
{
	/*struct can_frame frame;
	frame.can_id = 0x123;
	frame.can_dlc = 2;
	frame.data[0] = 0x11;
	frame.data[1] = 0x22;*/
    int nbytes;

	nbytes = write(_socket, &frame, sizeof(struct can_frame));

	printf("Wrote %d bytes\n", nbytes);

	return nbytes;
}

int CAN_Utils::writeFrameTo(string can_dest, struct can_frame frame)
{
	int nbytes;

	strcpy(ifr.ifr_name, "can0");
	ioctl(_socket, SIOCGIFINDEX, &ifr);
	addr.can_ifindex = ifr.ifr_ifindex;
	addr.can_family = AF_CAN;

	nbytes = sendto(_socket, &frame, sizeof(struct can_frame),
					0, (struct sockaddr *)&addr, sizeof(addr));

    return nbytes;
}

int CAN_Utils::readFrame(struct can_frame *frame)
{
	int nbytes;
	//struct can_frame frame;

	//nbytes = read(_socket, &frame, sizeof(struct can_frame));
	nbytes = read(_socket, frame, sizeof(struct can_frame));

	if (nbytes < 0)
	{
		perror("can raw socket read\n");
		return (-1);
	}

	/* paranoid check ... */
	if (nbytes < sizeof(struct can_frame))
	{
		fprintf(stderr, "read: incomplete CAN frame\n");
		return (-1);
	}

	/* do something with the received CAN frame */
	return nbytes;
}

int CAN_Utils::readFrameFrom(struct can_frame *frame, char* _ifr_name)
{
	int nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
	socklen_t len = sizeof(addr);
	//struct can_frame frame;

	/*nbytes = recvfrom(_socket, &frame, sizeof(struct can_frame),
					  0, (struct sockaddr *)&addr, &len);*/
    nbytes = recvfrom(_socket, frame, sizeof(struct can_frame),
					  0, (struct sockaddr *)&addr, &len);

	/* get interface name of the received CAN frame */
	ifr.ifr_ifindex = addr.can_ifindex;
	ioctl(_socket, SIOCGIFNAME, &ifr);

	printf("Received a CAN frame from interface %s\n", ifr.ifr_name);
	strcpy(_ifr_name, ifr.ifr_name);

	return nbytes;
}

int CAN_Utils::open(string ifname_str)
{
	//const char *ifname = "vcan0";

	// Open Socket for CAN Communication
	if ((_socket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		perror("Error while opening socket\n");
		return (-1);
	}

	// Determine th interface index
	//strcpy(ifr.ifr_name, ifname);
	strcpy(ifr.ifr_name, ifname_str.c_str());
	ioctl(_socket, SIOCGIFINDEX, &ifr);

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	// To bind a socket to all(!) CAN interfaces the interface index must be 0 (zero).
	printf("%s at index %d\n", ifr.ifr_name, ifr.ifr_ifindex);

	// To bind to socket
	if (bind(_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Error in socket bind\n");
		return (-2);
	}

	return (0);
}

int CAN_Utils::close()
{
	// close(_socket);
	return (0);
}

CAN_Utils::CAN_Utils()
{
}

} //namespace coms
} //namespace mechaspin
