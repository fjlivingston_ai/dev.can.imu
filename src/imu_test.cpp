#include "can_utils_lib.h"
#include <iostream>
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>
#include <sstream>
#include <iomanip>
#include "half.h"


static struct can_frame frame;

union {
    int16_t i;
    unsigned char b[2];
 } u;


/* Data Scale Factor for CAN bus data
 * Timestamp: 0.0025 (seconds)
 * Gyro: 1000  (rad)
 * Acc: 1000  (g)
 * Mag: 100  (uT)
 * AngularVel: 1000 (rad)
 * Quaternion: 10000
 * Euler: 10000 (rad)
 * LinearAcc: 1000 (g)
 * Pressure: 1000 (kPa)
 * Altitude: 10 (m)
 * Temperature: 100 (deg Cel) */
#define SF_GYRO  1000.0
#define SF_ACC	 1000.0
#define SF_MAG	 100.0
#define SF_VEL	 1000.0
#define SF_QUAT	 10000.0
#define SF_EULER 10000.0
#define SF_LACC	 1000.0
#define SF_PRES	 1000.0
#define SF_ALT 	 10.0.0
#define SF_TEMP  100.0

struct IMUData
{
	float x_g;   // Gyroscope X
	float y_g;   // Gyroscope Y
	float z_g;   // Gyroscope Z
	float p_rad; // Euler Angle X
	float r_rad; // Euler Angle Y
	float y_rad; // Euler Angle Z
	float x_a;   // Lin Acceleration X
	float y_a;   // Lin Acceleration Y
	float z_a;   // Lin Acceleration Z
	float x_mag; // Magnetometer X
	float y_mag; // Magnetometer Y
	float z_mag; // Magnetometer Z
	float qw;	// Quaternion W
	float qx;	// Quaternion X
	float qy;	// Quaternion Y
	float qz;	// Quaternion Z
};

float parseFloat(uint8_t *data, float denominator)
{
const int16_t temp = int16_t(data[0] + data[1] * 256);
return static_cast<float>(temp) / denominator;
}

void parseIMUHelper1(unsigned char *buf, IMUData* imu_data_id1)
{

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[6]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[7]) << " " << std::endl;
	imu_data_id1->p_rad = parseFloat(&buf[6], SF_EULER);
	std::cout << "p_rad: " << imu_data_id1->p_rad << std::endl;
}

void parseIMUHelper2(unsigned char *buf, IMUData* imu_data_id1)
{
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[0]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[1]) << " " << std::endl;
	imu_data_id1->r_rad = parseFloat(&buf[0], SF_EULER);
	std::cout << "r_rad: " << imu_data_id1->r_rad << std::endl;


	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[2]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[3]) << " " << std::endl;
	imu_data_id1->y_rad = parseFloat(&buf[2], SF_EULER);
	std::cout << "y_rad: " << imu_data_id1->y_rad << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[4]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[5]) << " " << std::endl;
	imu_data_id1->x_a = parseFloat(&buf[4], SF_ACC);
	std::cout << "x_a: " << imu_data_id1->x_a << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[6]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[7]) << " " << std::endl;
	imu_data_id1->y_a = parseFloat(&buf[6], SF_ACC);
	std::cout << "y_a: " << imu_data_id1->y_a << std::endl;
}

void parseIMUHelper3(unsigned char *buf, IMUData* imu_data_id1)
{
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[0]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[1]) << " " << std::endl;
	imu_data_id1->z_a = parseFloat(&buf[0], SF_ACC);
	std::cout << "z_a: " << imu_data_id1->z_a << std::endl;

}

void parseIMUHelper4_imu1(unsigned char *buf, IMUData* imu_data_id1)
{
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[0]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[1]) << " " << std::endl;
	imu_data_id1->qw = parseFloat(&buf[0], SF_QUAT);
	std::cout << "q_w: " << imu_data_id1->qw << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[2]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[3]) << " " << std::endl;
	imu_data_id1->qx = parseFloat(&buf[2], SF_QUAT);
	std::cout << "q_x: " << imu_data_id1->qx << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[4]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[5]) << " " << std::endl;
	imu_data_id1->qy = parseFloat(&buf[4], SF_QUAT);
	std::cout << "q_y: " << imu_data_id1->qy << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[6]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[7]) << " " << std::endl;
	imu_data_id1->qz = parseFloat(&buf[6], SF_QUAT);
	std::cout << "q_z: " << imu_data_id1->qz << std::endl;
}


void parseIMUHelper4_imu2(unsigned char *buf, IMUData* imu_data_id2)
{
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[0]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[1]) << " " << std::endl;
	imu_data_id2->qw = parseFloat(&buf[0], SF_QUAT);
	std::cout << "q_w: " << imu_data_id2->qw << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[2]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[3]) << " " << std::endl;
	imu_data_id2->qx = parseFloat(&buf[2], SF_QUAT);
	std::cout << "q_x: " << imu_data_id2->qx << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[4]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[5]) << " " << std::endl;
	imu_data_id2->qy = parseFloat(&buf[4], SF_QUAT);
	std::cout << "q_y: " << imu_data_id2->qy << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[6]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[7]) << " " << std::endl;
	imu_data_id2->qz = parseFloat(&buf[6], SF_QUAT);
	std::cout << "q_z: " << imu_data_id2->qz << std::endl;
}


void parseIMUHelper4_imu3(unsigned char *buf, IMUData* imu_data_id3)
{
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[0]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[1]) << " " << std::endl;
	imu_data_id3->qw = parseFloat(&buf[0], SF_QUAT);
	std::cout << "q_w: " << imu_data_id3->qw << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[2]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[3]) << " " << std::endl;
	imu_data_id3->qx = parseFloat(&buf[2], SF_QUAT);
	std::cout << "q_x: " << imu_data_id3->qx << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[4]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[5]) << " " << std::endl;
	imu_data_id3->qy = parseFloat(&buf[4], SF_QUAT);
	std::cout << "q_y: " << imu_data_id3->qy << std::endl;

	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[6]) << " ";
	std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(buf[7]) << " " << std::endl;
	imu_data_id3->qz = parseFloat(&buf[6], SF_QUAT);
	std::cout << "q_z: " << imu_data_id3->qz << std::endl;
}



void printIMUData(IMUData imu_data)
{
	std::cout << "Xg: " << imu_data.x_g << " Yg: " << imu_data.y_g << " Yg: " << imu_data.z_g << std::endl;
	std::cout << "p_rad: " << imu_data.p_rad << " r_rad: " << imu_data.p_rad << " y_rad: " << imu_data.y_rad << std::endl;
	std::cout << "Xa: " << imu_data.x_a << " Ya: " << imu_data.y_a << " Za: " << imu_data.z_a << std::endl;
	std::cout << "Xm: " << imu_data.x_mag << " Ym: " << imu_data.y_mag << " Zm: " << imu_data.z_mag << std::endl;
	std::cout << "qw: " << imu_data.qw << " qx: " << imu_data.qx << " qy: " << imu_data.qy << " qz: " << imu_data.qz << std::endl;
}

int main(int argc, char **argv)
{
	mechaspin::coms::CAN_Utils can_utils;
	bool quit = false;
	IMUData imu_data_id1;
	IMUData imu_data_id2;
	IMUData imu_data_id3;

	if (argc < 2)
	{
		std::cout << "usage: can_dev";
		return (1);
	}

	char *_ifr_name;
	strcpy(_ifr_name, argv[1]);

	// Open CAN Interface
	can_utils.open(_ifr_name); // can_utils.open("can0");

	while (!quit)
	{
		if (can_utils.readFrameFrom(&frame, _ifr_name))
		{
			int payload_length = frame.can_dlc; // frame payload length in byte (0 .. 8)
			int can_id = frame.can_id;			// frame payload id

			//IMUData imu_data = parseIMU(frame.data);
            // std::cout << "Interface: " << _ifr_name << std::endl;
			// std::cout << "Frame Length: " << payload_length << std::endl;
			std::cout << "CAN ID: " << can_id << std::endl;
			std::cout << "DATA (hex): ";

			for(int i = 0; i < payload_length; i++)
			{
				std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(frame.data[i]) << " ";
			}
			std::cout << std::endl;

			switch(can_id){
				// Euler_X (pitch)
				case 0x181:
				// parseIMUHelper1(frame.data, &imu_data);
				break;
				// Euler_Y and Z (yaw and roll), and Accel_x,y
				case 0x281:
				// parseIMUHelper2(frame.data, &imu_data);
				break;
        // Accel_Z
				case 0x381: 
				// parseIMUHelper3(frame.data, &imu_data);
				break;

				case 0x701: // receivied last message
				// printIMUData(imu_data);	
				break;

				// Quaternion
				case 0x481:
				parseIMUHelper4_imu1(frame.data, &imu_data_id1);
				break;

				case 0x482:
				parseIMUHelper4_imu2(frame.data, &imu_data_id2);
				break;

				case 0x483:
				parseIMUHelper4_imu3(frame.data, &imu_data_id3);
				break;


			}
				
		}

		// TODO: Add some sleep to match Sampling rate (100 Hz)
		boost::this_thread::sleep(boost::posix_time::microseconds(100));
	}

	return (0);
}

