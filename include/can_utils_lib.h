
#ifndef _CAN_UTILS_LIB_H
#define _CAN_UTILS_LIB_H

#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

namespace mechaspin
{
namespace coms
{

using namespace std;

class CAN_Utils
{

public:
	CAN_Utils();
	int open(string ifname_str);
	int close();

	int writeFrame(struct can_frame frame);
	int writeFrameTo(string can_dest, struct can_frame frame);
	int readFrame(struct can_frame *frame);
	int readFrameFrom(struct can_frame *frame, char* _ifr_name);

private:
	int _socket;
	
	struct sockaddr_can addr;
	struct ifreq ifr;
	string ifname_str;
};

} // namespace coms
} // namespace mechaspin

#endif
